package common

import model.RestApi._
import spray.json.DefaultJsonProtocol._

object RestApiImplicits {

  implicit val anyErrFormat = jsonFormat1(AnyErr)

}
