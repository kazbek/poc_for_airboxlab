package common

import com.typesafe.config.ConfigFactory


object Settings {

  private val config = ConfigFactory.load()

  val actorSystemName: String = config.getString("akka.actor-system")
  val actorBackendName: String = config.getString("akka.backend-actor-cluster-listener-name")
  val actorFrontendName: String = config.getString("akka.frontend-actor-cluster-listener-name")

  val cassandraAddress: Array[String] = scala.util.Properties.envOrElse("CASSANDRA_ADDRESS", "localhost").split(",")
  val cassandraPort = scala.util.Properties.envOrElse("CASSANDRA_PORT", "9042").toInt
  val cassandraKeyspace: String = scala.util.Properties.envOrElse("CASSANDRA_KEYSPACE", "airboxlab")
  val cassandraLogin: String = scala.util.Properties.envOrElse("CASSANDRA_LOGIN", "")
  val cassandraPassword: String = scala.util.Properties.envOrElse("CASSANDRA_PASSWORD", "")

  val webserverAddress = scala.util.Properties.envOrElse("WEBSERVER_ADDRESS", "0.0.0.0")
  val webserverPort = scala.util.Properties.envOrElse("WEBSERVER_PORT", "8112").toInt

  val filesystemPath = scala.util.Properties.envOrElse("FILESYSTEM_PATH", "/home")



}
