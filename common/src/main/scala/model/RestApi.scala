package model

import java.util.UUID

object RestApi {

  trait Requests
  trait DbRequests extends Requests

  trait SensorDbRequests extends DbRequests
  case class RqGetSensor(sensorId: Option[UUID]) extends SensorDbRequests
  case class RqSetSensor(sensor: Sensor) extends SensorDbRequests
  case class RqGetSensorData(sensorId: UUID) extends SensorDbRequests
  case class RqSetSensorData(sensorData: SensorData) extends SensorDbRequests

  trait UserDbRequests extends DbRequests
  case class RqDbUser(login: String) extends UserDbRequests
  case class RqDbUserToken(token: UUID) extends UserDbRequests




  trait Responses
  case object Ok extends Responses

  trait Err extends Responses
  case object UnhandledDbTask extends Err
  case class AnyErr(message: String) extends Err

  trait DBResponses extends Responses
  case class AnyOk(u: Unit) extends DBResponses
  case class RsDbUser(dbUser: Option[DbUser]) extends DBResponses
  case class RsDbUserToken(dbUserToken: Option[DbUserToken]) extends DBResponses

  case class RsDbSensor(sensors: Seq[Sensor]) extends DBResponses
  case class RsDbSensorData(sensorsData: Seq[SensorData]) extends DBResponses
}