package model

import java.util.UUID

import model.SensorMeasurement.{GenericMeasurement, Measurement}


// CREATE TABLE airboxlab.sensor (id uuid, name varchar, PRIMARY KEY (id));
case class Sensor(id: UUID, name: String) extends Cassandraable {
  override def toString: String = s"Sensor: {id:$id, name:$name}"
  override def values = Array [AnyRef]( id, name )
}
object Sensor {
  val tableName = "sensor"
  val cols = Array("id", "name")
}




// CREATE TABLE airboxlab.sensor_data (sensor_id uuid, ts timestamp, ms_type int, measurement decimal, PRIMARY KEY (sensor_id));
case class SensorData(sensorId: UUID, genericMeasurement: GenericMeasurement) extends Cassandraable {
  override def toString: String = s"SensorData: {sensorId:$sensorId, genericMeasurement:{$genericMeasurement}}"

  override def values = Array[AnyRef](
    sensorId,
    genericMeasurement.timeStamp.getMillis.asInstanceOf[AnyRef],
    genericMeasurement.measurementType.asInstanceOf[AnyRef],
    genericMeasurement.measurement.bigDecimal.asInstanceOf[AnyRef]
  )
}
object SensorData {
  val tableName = "sensor_data"
  val cols = Array("sensor_id", "ts", "ms_type", "measurement")
}