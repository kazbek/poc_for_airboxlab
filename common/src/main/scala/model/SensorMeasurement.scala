package model

import org.joda.time.DateTime

object SensorMeasurement {

  case class GenericMeasurement(private val value: BigDecimal, private val mt: Int, private val ts: DateTime = DateTime.now) extends Measurement[BigDecimal] {
    override val timeStamp = ts
    override val measurement = value
    override val measurementType = mt
  }

  trait Measurement[T] {
    val timeStamp: DateTime
    val measurementType: Int
    val measurement: T

    def getGenericValue: BigDecimal = measurement match {
      case b: Boolean => if (b) 1 else 0
      case i: Int => BigDecimal(i)
      case _ => measurement.asInstanceOf[BigDecimal]
    }

    override def toString: String = s"Measurement: {measurementType:$measurementType, measurement:$getGenericValue, timeStamp: $timeStamp}"

    def getGenericMeasurement = GenericMeasurement(getGenericValue, measurementType, timeStamp)
  }


  object Simple {
    case class OnOff(private val value: Boolean, private val ts: DateTime = DateTime.now) extends Measurement[Boolean] {
      override val timeStamp = ts
      override val measurement = value
      override val measurementType = 0
    }

    case class Temperature(private val value: BigDecimal, private val ts: DateTime = DateTime.now) extends Measurement[BigDecimal] {
      override val timeStamp = ts
      override val measurement = value
      override val measurementType = 1
    }

    case class Speed(private val value: BigDecimal, private val ts: DateTime = DateTime.now) extends Measurement[BigDecimal] {
      override val timeStamp = ts
      override val measurement = value
      override val measurementType = 2
    }

    case class Weight(private val value: BigDecimal, private val ts: DateTime = DateTime.now) extends Measurement[BigDecimal] {
      override val timeStamp = ts
      override val measurement = value
      override val measurementType = 3
    }

    case class Distance(private val value: BigDecimal, private val ts: DateTime = DateTime.now) extends Measurement[BigDecimal] {
      override val timeStamp = ts
      override val measurement = value
      override val measurementType = 4
    }

    case class Pressure(private val value: BigDecimal, private val ts: DateTime = DateTime.now) extends Measurement[BigDecimal] {
      override val timeStamp = ts
      override val measurement = value
      override val measurementType = 5
    }
  }


  object Logical {

    // -∞, 1 = 1
    //     0 = 0
    // 2, +∞ = 2
    case class OneZeroTwo(private val value: Int, private val ts: DateTime = DateTime.now) extends Measurement[Int] {
      override val timeStamp = ts
      override val measurement =
        if (value < 0) 1
        else if (value > 1) 2
        else value
      override val measurementType = 1001
    }

    case class Switcher(private val value: Int, private val ts: DateTime = DateTime.now) extends Measurement[Int] {
      override val timeStamp = ts
      override val measurement = value
      override val measurementType = 1002
    }


  }







}



