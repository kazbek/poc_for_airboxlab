package model

trait Cassandraable {
  def values: Array[AnyRef]
}


// CREATE KEYSPACE airboxlab WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };

// CREATE TABLE airboxlab.db_user (login varchar, password varchar, permissions varchar, token_id uuid, token_created_on timestamp, PRIMARY KEY (login));
// CREATE TABLE airboxlab.db_user_token (token_id uuid, login varchar, permissions varchar, token_created_on timestamp, PRIMARY KEY (token_id));
// UPDATE airboxlab.db_user SET password = 'DAc8PbN6jeCfUQptA36895BhDMFmKnLcnUmM', permissions = 'ADMIN,VIEW_PERMISSIONS', token_created_on = 1515568890123, token_id = 7d6e72e7-b388-4701-8c74-754fbc5a00e0 where login = 'admin' ;
// UPDATE airboxlab.db_user SET password = 'doe', permissions = 'VIEW_PERMISSIONS', token_created_on = 1515568890123, token_id = e96fb80e-82d9-4bc5-9e1b-dcb437f07a4d where login = 'john' ;
// UPDATE airboxlab.db_user_token SET login = 'admin', permissions = 'ADMIN,VIEW_PERMISSIONS', token_created_on = 1515568890123 where token_id = 7d6e72e7-b388-4701-8c74-754fbc5a00e0;
// UPDATE airboxlab.db_user_token SET login = 'john', permissions = 'VIEW_PERMISSIONS', token_created_on = 1515568890123 where token_id = e96fb80e-82d9-4bc5-9e1b-dcb437f07a4d;

// CREATE TABLE airboxlab.sensor (id uuid, name varchar, PRIMARY KEY (id));
// CREATE TABLE airboxlab.sensor_data (sensor_id uuid, ts timestamp, ms_type int, measurement decimal, PRIMARY KEY ((sensor_id), ts, ms_type));
