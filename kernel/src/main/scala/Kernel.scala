import akka.actor._
import kamon.Kamon

import scala.concurrent.Await
import scala.concurrent.duration.Duration


/** How to Run from bash

export AKKA_SEED_NODES = '0.0.0.0:2771,0.0.0.0:2772'
export AKKA_REMOTING_BIND_HOST = "0.0.0.0"
export AKKA_REMOTING_BIND_PORT = 2771
export AKKA_ACTOR_SYSTEM_NAME = 'FobotClusterSystem'


sbt run java \
-Dakka.actor.provider=cluster \
-Dakka.remote.netty.tcp.hostname="$(eval "echo $AKKA_REMOTING_BIND_HOST")" \
-Dakka.remote.netty.tcp.port="$AKKA_REMOTING_BIND_PORT" $(IFS=','; I=0; for NODE in $AKKA_SEED_NODES; do echo " \
-Dakka.cluster.seed-nodes.$I=akka.tcp://$AKKA_ACTOR_SYSTEM_NAME@$NODE"; I=$(expr $I + 1); done) \
-DactorSystemName=${AKKA_ACTOR_SYSTEM_NAME}

  */



// sbt run java -Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false
// ./akka-cluster 127.0.0.1 9999 cluster-status
// akka-cluster 127.0.0.1 9999 down akka.tcp://FobotClusterSystem@127.0.0.1:49603
// sbt aspectj-runner:run -Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false
// java -Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -jar backend-workers-assembly-1.0.jar



object Kernel extends App {

  val actorSystemName =
    sys.props.getOrElse(
      "actorSystemName",
      throw new IllegalArgumentException("Actor system name must be defined by the actorSystemName property")
    )

  val actorSystem = ActorSystem(actorSystemName)

  Kamon.start()

  Await.result(actorSystem.whenTerminated, Duration.Inf)

}
