import actors.BackendClusterListener
import akka.actor._
import common.Settings._
import kamon.Kamon

import scala.concurrent.Await
import scala.concurrent.duration.Duration


object Worker extends App {

  Kamon.start()

  import common.AkkaImplicits._

  system.actorOf(Props[BackendClusterListener], actorBackendName)

  Await.result(system.whenTerminated, Duration.Inf)

}