package Common

import akka.actor.ActorRef
import model.RestApi.{AnyErr, DBResponses}

import scala.util.{Failure, Success, Try}

object CustomExtentions {

  implicit class SendersExtentions(private val sender: ActorRef) extends AnyVal {
    def tryTo (tryValue: Try[DBResponses]) =
      tryValue match {
        case Success(response) => sender ! response
        case Failure(ex) =>
          println(s"Exception: $ex")
          sender ! AnyErr(ex.getMessage)
      }
  }

}
