package actors

import akka.actor.{Actor, ActorLogging}
import data.CassandraRepositoryJavaDriver
import kamon.trace.Tracer
import model.RestApi._
import scala.util.Try

class DbActor extends Actor with ActorLogging {

  lazy val repositoryCassyJavaDriver = new CassandraRepositoryJavaDriver

  import Common.CustomExtentions.SendersExtentions

  override def receive: Receive = {

    case RqDbUser(login) =>
      Tracer.withNewContext("RqDbUser comes", autoFinish = true) {
        log.info("RqDbUser comes.")
        sender tryTo Try(RsDbUser(repositoryCassyJavaDriver.getDbUser(login)))
      }

    case RqDbUserToken(token) =>
      Tracer.withNewContext("RqDbUserToken comes", autoFinish = true) {
        log.info("RqDbUserToken comes.")
        sender tryTo Try(RsDbUserToken(repositoryCassyJavaDriver.getDbUserToken(token)))
      }


    case RqGetSensor(sensorId) =>
      Tracer.withNewContext("RqGetSensor comes", autoFinish = true) {
        log.info("RqGetSensor comes.")
        sender tryTo Try(RsDbSensor(repositoryCassyJavaDriver.getSensor(sensorId)))
      }
    case RqSetSensor(sensor) =>
      Tracer.withNewContext("RqSetSensor comes", autoFinish = true) {
        log.info(s"RqSetSensor comes. $sensor")
        sender tryTo Try(AnyOk(repositoryCassyJavaDriver.setSensor(sensor)))
      }
    case RqGetSensorData(sensorId) =>
      Tracer.withNewContext("RqGetSensorData comes", autoFinish = true) {
        log.info("RqGetSensorData comes.")
        sender tryTo Try(RsDbSensorData(repositoryCassyJavaDriver.getSensorData(sensorId)))
      }
    case RqSetSensorData(sensorData) =>
      Tracer.withNewContext("RqSetSensorData comes", autoFinish = true) {
        log.info(s"RqSetSensorData comes. $sensorData")
        sender tryTo Try(AnyOk(repositoryCassyJavaDriver.setSensorData(sensorData)))
      }



    case _ =>
      Tracer.withNewContext("Something comes", autoFinish = true) {
        log.info("Something comes.")
      }
  }

}