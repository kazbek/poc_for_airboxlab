package actors

import akka.actor.{Actor, ActorLogging, Props, RootActorPath}
import akka.cluster.ClusterEvent._
import akka.cluster.{Cluster, Member, MemberStatus}
import akka.routing.RoundRobinPool
import common.AkkaImplicits.system
import common.Settings._
import model.AkkaObjects.BackendRegistration
import model.RestApi._

class BackendClusterListener extends Actor with ActorLogging {

  val cluster = Cluster(context.system)
  lazy val dbActor = system.actorOf(
    Props(new DbActor).withRouter(RoundRobinPool(nrOfInstances = 100)), "dbActor")

  // subscribe to cluster changes, re-subscribe when restart
  override def preStart(): Unit = cluster.subscribe(self, classOf[MemberUp])

  override def postStop(): Unit = cluster.unsubscribe(self)

  override def receive = {

    case state: CurrentClusterState =>
      state.members.filter(_.status == MemberStatus.Up) foreach register

    case MemberUp(member) =>
      log.info(s"[Listener] node is up: $member")
      register(member)

    case request: DbRequests =>
      log.info(s"DataBase job comes.")
      dbActor forward request
  }

  def register(member: Member): Unit =
    if (member.hasRole("frontend"))
      context.actorSelection(RootActorPath(member.address) / "user" / actorFrontendName) !
        BackendRegistration

}
