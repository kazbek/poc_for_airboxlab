package data

import java.lang.System.nanoTime
import java.util.UUID

import com.datastax.driver.core._
import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.dse.auth.DsePlainTextAuthProvider
import com.google.common.cache.{CacheBuilder, CacheLoader, LoadingCache}
import common.Settings._
import kamon.trace.Tracer
import model.SensorMeasurement.GenericMeasurement
import model._
import org.joda.time.{DateTime, DateTimeZone}

import scala.collection.JavaConverters._

class CassandraRepositoryJavaDriver {

  private def profile[R](code: => R, t: Long = nanoTime): (R, Long) = (code, nanoTime - t)

  val nrOfCacheEntries: Int = 100
  val poolingOptions = new PoolingOptions
  val cluster: Cluster = Cluster.builder()
    .withAuthProvider(new DsePlainTextAuthProvider(cassandraLogin, cassandraPassword))
    .addContactPoints(cassandraAddress: _*)
    .withPort(cassandraPort)
    .withPoolingOptions(poolingOptions)
    .build()
  val session: Session = cluster.newSession()
  val cache: LoadingCache[String, PreparedStatement] =
    CacheBuilder.newBuilder().
      maximumSize(nrOfCacheEntries).
      build(
        new CacheLoader[String, PreparedStatement]() {
          def load(key: String): PreparedStatement = session.prepare(key.toString)
        }
      )


  object CassandraObject {

    def toDateTime(row: Row, col: String): Option[DateTime] =
      if (row.getObject(col) == null) None
      else Some(new DateTime(row.getTimestamp(col).getTime, DateTimeZone.getDefault))

    def toUUID(row: Row, col: String): Option[UUID] =
      if (row.getObject(col) == null) None
      else Some(row.getUUID(col))

    def toString(row: Row, col: String): Option[String] =
      if (row.getObject(col) == null) None
      else Some(row.getString(col))

    def toBigDecimal(row: Row, col: String): Option[BigDecimal] =
      if (row.getObject(col) == null) None
      else Some(row.getDecimal(col))


    def getDbUser(cache: LoadingCache[String, PreparedStatement], session: Session)(login: String): Option[DbUser] = {
      val query: Statement =
        QueryBuilder.select().
          all().
          from(cassandraKeyspace, DbUser.tableName).
          where(QueryBuilder.eq("login", QueryBuilder.bindMarker()))

      session.execute(cache.get(query.toString).bind(login)).all().asScala.map(row => DbUser(
        row.getString("login"),
        row.getString("password"),
        row.getString("permissions"),
        row.getUUID("token_id"),
        toDateTime(row, "token_created_on").get))
        .headOption
    }
    val getDbUser: String => Option[DbUser] = getDbUser(cache, session)


    def getDbUserToken(cache: LoadingCache[String, PreparedStatement], session: Session)(token: UUID): Option[DbUserToken] = {
      val query: Statement =
        QueryBuilder.select().
          all().
          from(cassandraKeyspace, DbUserToken.tableName).
          where(QueryBuilder.eq("token_id", QueryBuilder.bindMarker()))

      session.execute(cache.get(query.toString).bind(token)).all().asScala.map(row => DbUserToken(
        row.getUUID("token_id"),
        row.getString("login"),
        row.getString("permissions"),
        toDateTime(row, "token_created_on").get))
        .headOption
    }
    val getDbUserToken: UUID => Option[DbUserToken] = getDbUserToken(cache, session)


    def getSensor(cache: LoadingCache[String, PreparedStatement], session: Session)(id: Option[UUID]): Seq[Sensor] = {
      val query = QueryBuilder.select ().all ().from ( cassandraKeyspace, Sensor.tableName )
      val rows = id match {
        case Some ( id ) =>
          session.execute ( cache.get ( query.where ( QueryBuilder.eq ( "id", QueryBuilder.bindMarker () ) ).toString ).bind ( id ) ).all ()
        case None =>
          session.execute ( cache.get ( query.toString ).bind () ).all ()
      }

      rows.asScala.map ( row => Sensor (
        id = row.getUUID("id"),
        name = row.getString("name")
      ) )
    }
    val getSensor: Option[UUID] => Seq[Sensor] = getSensor(cache, session)

    def getSensorData(cache: LoadingCache[String, PreparedStatement], session: Session)(sensorId: UUID): Seq[SensorData] = {
      val query: Statement =
        QueryBuilder.select().
          all().
          from(cassandraKeyspace, SensorData.tableName).
          where(QueryBuilder.eq("sensor_id", QueryBuilder.bindMarker()))

      session.execute(cache.get(query.toString).bind(sensorId)).all().asScala.map(row => SensorData(
        sensorId = row.getUUID("sensor_id"),
        genericMeasurement = GenericMeasurement(
          value = row.getDecimal("measurement"),
          mt = row.getInt("ms_type"),
          ts = new DateTime(row.getTimestamp("ts").getTime, DateTimeZone.getDefault)
        )
      ))
    }
    val getSensorData: UUID => Seq[SensorData] = getSensorData(cache, session)
  }

  def getDbUser(login: String): Option[DbUser] =
    Tracer.withNewContext("CassandraRepositoryJavaDriver___getDbUser", autoFinish = true) {
      CassandraObject.getDbUser(login)
    }
  def setDbUser(dbUser: DbUser): Unit =
    Tracer.withNewContext("CassandraRepositoryJavaDriver___setDbUser", autoFinish = true) {
      session.execute(
        QueryBuilder.insertInto(cassandraKeyspace, DbUser.tableName).values(DbUser.cols, dbUser.values))
    }

  def getDbUserToken(token: UUID): Option[DbUserToken] =
    Tracer.withNewContext("CassandraRepositoryJavaDriver___getDbUserToken", autoFinish = true) {
      CassandraObject.getDbUserToken(token)
    }
  def setDbUserToken(dbUserToken: DbUserToken): Unit =
    Tracer.withNewContext("CassandraRepositoryJavaDriver___setDbUserToken", autoFinish = true) {
      session.execute(
        QueryBuilder.insertInto(cassandraKeyspace, DbUserToken.tableName).values(DbUserToken.cols, dbUserToken.values))
    }



  def getSensor(id: Option[UUID]): Seq[Sensor] =
    Tracer.withNewContext("CassandraRepositoryJavaDriver___getSensor", autoFinish = true) {
      CassandraObject.getSensor(id)
    }
  def setSensor(sensor: Sensor): Unit =
    Tracer.withNewContext("CassandraRepositoryJavaDriver___setSensor", autoFinish = true) {
      session.execute(
        QueryBuilder.insertInto(cassandraKeyspace, Sensor.tableName).values(Sensor.cols, sensor.values))
    }

  def getSensorData(sensorId: UUID): Seq[SensorData] =
    Tracer.withNewContext("CassandraRepositoryJavaDriver___getSensorData", autoFinish = true) {
      CassandraObject.getSensorData(sensorId)
    }
  def setSensorData(sensorData: SensorData): Unit =
    Tracer.withNewContext("CassandraRepositoryJavaDriver___setSensorData", autoFinish = true) {
      session.execute(
        QueryBuilder.insertInto(cassandraKeyspace, SensorData.tableName).values(SensorData.cols, sensorData.values))
    }


}
