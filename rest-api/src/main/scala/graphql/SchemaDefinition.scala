package graphql

import java.util.UUID

import model.SensorMeasurement.GenericMeasurement
import model._
import org.joda.time.DateTime
import sangria.schema._
import sangria.validation.ValueCoercionViolation

object SensorsModel {

  case object IDViolation extends ValueCoercionViolation("Invalid ID")
  val UUIDType = ScalarAlias[UUID, String](IDType,
    toScalar = _.toString,
    fromScalar = idString ⇒ try Right(UUID.fromString(idString)) catch {
      case _: IllegalArgumentException ⇒ Left(IDViolation)
    })


  case object DateTimeViolation extends ValueCoercionViolation("Invalid DateTime")
  val DateTimeType = ScalarAlias[DateTime, String](StringType,
    toScalar = _.toString,
    fromScalar = dataTimeString ⇒ try Right(DateTime.parse(dataTimeString)) catch {
      case _: IllegalArgumentException ⇒ Left(DateTimeViolation)
    })


  val MeasurementTypeEnum = EnumType (
    "MeasurementTypeEnum",
    Some ( "Measurement Type" ),
    List (
      EnumValue ( "OnOff", value = 0, description = Some ( "On / Off (Boolean type)" ) ),
      EnumValue ( "Temperature", value = 1, description = Some ( "Temperature (BigDecimal type)" ) ),
      EnumValue ( "Speed", value = 2, description = Some ( "Speed (BigDecimal type)" ) ),
      EnumValue ( "Weight", value = 3, description = Some ( "Weight (BigDecimal type)" ) ),
      EnumValue ( "Distance", value = 4, description = Some ( "Distance (BigDecimal type)" ) ),
      EnumValue ( "Pressure", value = 5, description = Some ( "Pressure (BigDecimal type)" ) ),
      EnumValue ( "OneZeroTwo", value = 1001, description = Some ( "One / Off / Two (values: 1 0 2)" ) ),
      EnumValue ( "Switcher", value = 1002, description = Some ( "Switcher (Int type)" ) )
    )
  )

  val SensorType: ObjectType[SecureContext, Sensor] = ObjectType ( "Sensor", "Sensor",
    () ⇒ fields [SecureContext, Sensor](
      Field ( "id", UUIDType, Some ( "Sensor's unique identifier" ), resolve = _.value.id ),
      Field ( "name", StringType, Some ( "Sensor's name" ), resolve = _.value.name ),
      Field ( "sensorData", ListType ( SensorDataType ), Some ( "Sensor's Data" ),
        resolve = ctx ⇒ ctx.ctx.repo.getSensorsData ( ctx.value.id ) )
    ) )

  val SensorDataType: ObjectType[SecureContext, SensorData] = ObjectType ( "SensorData", "SensorData",
    () ⇒ fields [SecureContext, SensorData](
      Field ( "sensorId", UUIDType, Some ( "Sensor's unique identifier" ), resolve = _.value.sensorId ),
      Field ( "measurement", SensorMeasurementType, Some ( "Sensor's Measurement" ), resolve = _.value.genericMeasurement ),
    ) )

  val SensorMeasurementType: ObjectType[SecureContext, GenericMeasurement] = ObjectType ( "SensorMeasurement", "SensorMeasurement",
    () ⇒ fields [SecureContext, GenericMeasurement](
      Field ( "timeStamp", DateTimeType, Some ( "Measurement's TimeStamp" ), resolve = _.value.timeStamp ),
      Field ( "measurement", BigDecimalType, Some ( "Measurement's Value" ), resolve = _.value.measurement ),
      Field ( "measurementType", MeasurementTypeEnum, Some ( "Measurement's Type" ), resolve = _.value.measurementType )
    ) )

  val queryById = Argument ( "id", UUIDType, description = "Sensor's unique identifier" )
}

object QueryDefinition {

  val queries = ObjectType(
    "Query",
    "Sensors, SensorsData and etc...",
    fields[SecureContext, Unit](
      Field(
        name = "me",
        fieldType = OptionType(UserModel.UserType),
        resolve = ctx ⇒ ctx.ctx.authorised()(user ⇒ user)),

      Field(
        name = "getAllSensors",
        description = Some("Get All Sensors"),
        fieldType = ListType(SensorsModel.SensorType),
        arguments = Nil,
        resolve = ctx ⇒ ctx.ctx.repo.getSensors(None)),

      Field(
        name = "getSensorsById",
        description = Some("Get Sensor by Id"),
        fieldType = ListType(SensorsModel.SensorType),
        arguments = SensorsModel.queryById :: Nil,
        resolve = ctx ⇒ ctx.ctx.repo.getSensors(Some(ctx arg SensorsModel.queryById)))

    ))

}



object MutationDefinition {
  val mutations = ObjectType(
    "Mutation",
    "Create Users and etc...",
    fields[SecureContext, Unit](
      Field("login", OptionType(StringType),
        arguments = UserModel.UserNameArg :: UserModel.PasswordArg :: Nil,
        resolve = ctx ⇒ UpdateCtx(ctx.ctx.login(ctx.arg(UserModel.UserNameArg), ctx.arg(UserModel.PasswordArg))) { token ⇒
          ctx.ctx.copy(token = Some(token))
        })

    ))
}


object SchemaDefinition {

  val fileIOSchema = Schema ( query = QueryDefinition.queries, mutation = Some ( MutationDefinition.mutations ) )

}
