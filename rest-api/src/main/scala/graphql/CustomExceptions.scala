package graphql


object CustomExceptions {

  case class QLException(message: String) extends Exception(message)

  case class AuthenticationException(message: String) extends Exception(message)

  case class AuthorisationException(message: String) extends Exception(message)

}

