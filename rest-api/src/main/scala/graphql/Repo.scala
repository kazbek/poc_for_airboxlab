package graphql

import java.util.UUID

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import model.RestApi._
import model.{Sensor, SensorData}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class Repo(actorRef: ActorRef) {

  val timeout1Minutes = 1.minute
  implicit val timeout: Timeout = timeout1Minutes

  def getSensors(id: Option[UUID]): Future[Seq[Sensor]] =
    (actorRef ? RqGetSensor ( id )).map {
      case rsDbSensor: RsDbSensor => rsDbSensor.sensors
      case ex@_ => throw CustomExceptions.QLException(ex.toString)
    }

  def getSensorsData(sensorId: UUID): Future[Seq[SensorData]] =
    (actorRef ? RqGetSensorData ( sensorId )).map {
      case rsDbSensorData: RsDbSensorData => rsDbSensorData.sensorsData
      case ex@_ => throw CustomExceptions.QLException(ex.toString)
    }

}
