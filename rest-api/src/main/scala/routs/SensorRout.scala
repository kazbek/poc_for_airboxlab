package routs

import java.util.UUID

import akka.actor.ActorRef
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives.{get, path, _}
import akka.http.scaladsl.server.StandardRoute
import graphql.UserRepo
import model.RestApi.RqSetSensorData
import model.SensorData
import model.SensorMeasurement.Measurement
import model.SensorMeasurement.Simple._
import model.SensorMeasurement.Logical._

class SensorRout(frontendActor: ActorRef) extends BaseRout {

  val userRepo = new UserRepo(frontendActor)

  def process[T](sensorId: UUID, measurement: Measurement[T]): StandardRoute = {
    frontendActor ! RqSetSensorData(SensorData(sensorId, measurement.getGenericMeasurement))
    println(s"Save to DataBase: $measurement")
    complete(HttpEntity(ContentTypes.`text/html(UTF-8)`,
      s"<h2>sensorId: $sensorId</h2>" +
        s"<hr> " +
        s"<h3>Measurement</h3>" +
        s"<div style='padding-left:20px;'>" +
        s"<div>Type: ${measurement.measurementType}</div>" +
        s"<div>Class Name: ${measurement.getClass}</div>" +
        s"<div>TimeStamp: ${measurement.timeStamp}</div>" +
        s"<div>Value: ${measurement.getGenericValue}</div>" +
        s"</div>"
    ))
  }

  // http://localhost:8112/sensor/e96fb80e-82d9-4bc5-9e1b-dcb437f07a4d/OnOff/1
  // http://localhost:8112/sensor/e96fb80e-82d9-4bc5-9e1b-dcb437f07a4d/Temperature/36.6
  // http://localhost:8112/sensor/e96fb80e-82d9-4bc5-9e1b-dcb437f07a4d/Speed/119.99
  // http://localhost:8112/sensor/e96fb80e-82d9-4bc5-9e1b-dcb437f07a4d/Weight/94.56
  // http://localhost:8112/sensor/e96fb80e-82d9-4bc5-9e1b-dcb437f07a4d/Distance/2574.2
  // http://localhost:8112/sensor/e96fb80e-82d9-4bc5-9e1b-dcb437f07a4d/Pressure/120.80
  // http://localhost:8112/sensor/e96fb80e-82d9-4bc5-9e1b-dcb437f07a4d/OneZeroTwo/2
  // http://localhost:8112/sensor/e96fb80e-82d9-4bc5-9e1b-dcb437f07a4d/Switcher/7

  // query {
  //  getAllSensors {
  //    name
  //    sensorData {
  //      sensorId
  //      measurement {
  //        timeStamp
  //        measurement
  //        measurementType
  //      }
  //    }
  //  }
  //}

  val getRoute = pathPrefix("sensor" / JavaUUID) { sensorId =>
      path("OnOff" / IntNumber)(value => get(process(sensorId, OnOff(value != 0)))) ~
      path("Temperature" / DoubleNumber)(value => get(process(sensorId, Temperature(value)))) ~
      path("Speed" / DoubleNumber)(value => get(process(sensorId, Speed(value)))) ~
      path("Weight" / DoubleNumber)(value => get(process(sensorId, Weight(value)))) ~
      path("Distance" / DoubleNumber)(value => get(process(sensorId, Distance(value)))) ~
      path("Pressure" / DoubleNumber)(value => get(process(sensorId, Pressure(value)))) ~
      path("OneZeroTwo" / IntNumber)(value => get(process(sensorId, OneZeroTwo(value)))) ~
      path("Switcher" / IntNumber)(value => get(process(sensorId, Switcher(value))))
  }

}
