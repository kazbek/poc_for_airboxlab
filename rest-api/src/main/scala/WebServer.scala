import akka.actor._
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import routs._
import actors.FrontendClusterListener
import akka.http.scaladsl.model.headers.RawHeader
import common.Settings._
import kamon.Kamon


object WebServer extends App {

  Kamon.start()

  import common.AkkaImplicits._
  val frontendActor = system.actorOf(Props(new FrontendClusterListener), actorFrontendName)

  val sensor = new SensorRout(frontendActor)
  val ql = new QLRout(frontendActor)

  val headers = List(
    RawHeader("Access-Control-Allow-Origin", "*"),
    RawHeader("Access-Control-Allow-Credentials", "true"),
    RawHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, X-Requested-With")
  )
  val route = respondWithHeaders(headers) {
      sensor.getRoute ~ ql.getRoute
    }

  Http().bindAndHandle(route, webserverAddress, webserverPort)

  Await.result(system.whenTerminated, Duration.Inf)

}